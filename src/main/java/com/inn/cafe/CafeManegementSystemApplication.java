package com.inn.cafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafeManegementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CafeManegementSystemApplication.class, args);
	}

}
